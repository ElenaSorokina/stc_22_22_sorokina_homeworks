public class Main {
    public void main(String[] args) {
        ATM cash = new ATM();
        cash.balanceCash(10000);
        cash.maxWithdrawalAmount(5000);
        cash.maxPossibleAmount(20000);
        cash.giveCash(3000);
        cash.depositCash(7000);
        cash.isWorker = true;

        cash.grow(7000);
        cash.fullCash();

        cash.giveCash(3000);
        cash.fullCash();
        

    }

}