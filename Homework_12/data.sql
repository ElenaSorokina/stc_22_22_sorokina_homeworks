insert into пользователь (experience, number, age, driver_license, license_B, rating)
values (2, 678969, 23, true, true, 4);
insert into пользователь (experience, number, age, driver_license, license_B, rating)
values (4, 678969, 38, true, true, 3);
insert into пользователь (experience, number, age, driver_license, license_B, rating)
values (6, 678970, 18, true, true, 5);
insert into пользователь (experience, number, age, driver_license, license_B, rating)
values (1, 678955, 49, true, true, 5);
insert into пользователь (experience, number, age, driver_license, license_B, rating)
values (8, 678911, 47, true, true, 4);



insert into drive (id_car, date, length_drive)
values (01, '2022-10-02', 30);
insert into drive (id_car, date, length_drive)
values (02, '2022-10-03', 20);
insert into drive (id_car, date, length_drive)
values (03, '2022-10-04', 40);
insert into drive (id_car, date, length_drive)
values (04, '2022-10-05', 50);
insert into drive (id_car, date, length_drive)
values (05, '2022-10-06', 60);

insert into car (model, color, number)
values ('ford', 'black', 100);
insert into car (model, color, number)
values ('tesla', 'red', 200);
insert into car (model, color, number)
values ('volvo', 'blue', 300);
insert into car (model, color, number)
values ('suzuki', 'grey', 400);
insert into car (model, color, number)
values ('kia', 'white', 500);

insert into rent (id_owner_car, id_owner)
values (1, 4);
insert into rent (id_owner_car, id_owner)
values (2, 1);
insert into rent (id_owner_car, id_owner)
values (3, 2);
insert into rent (id_owner_car, id_owner)
values (4, 3);
insert into rent (id_owner_car, id_owner)
values (5, 5);

insert into пользователь_car_drive (id_owner_car, id_owner)
values (1, 1);
insert into пользователь_car_drive (id_owner_car, id_owner)
values (2, 2);
insert into пользователь_car_drive (id_owner_car, id_owner)
values (3, 3);
insert into пользователь_car_drive (id_owner_car, id_owner)
values (4, 4);
insert into пользователь_car_drive (id_owner_car, id_owner)
values (5, 5);