drop table if exists пользователь;

create table пользователь
(
    first_name     char(20) DEFAULT 'FIRST_NAME',
    experience     integer,
    last_name      char(20) DEFAULT 'LAST_NAME',
    number         integer,
    age            integer,
    driver_license bool,
    license_B      bool,
    rating         integer check (rating >= 0 and rating <= 5)
);



update пользователь
set first_name = 'Михаил',
    last_name  = 'Иванов'
where age = 23;
update пользователь
set first_name = 'Никита',
    last_name  = 'Сидоров'
where age = 38;
update пользователь
set first_name = 'Егор',
    last_name  = 'Донцов'
where age = 18;
update пользователь
set first_name = 'Роман',
    last_name  = 'Сорокин'
where age = 49;
update пользователь
set first_name = 'Владимир',
    last_name  = 'Соколов'
where age = 47;

create table car
(
    model    char(20),
    color    char(20),
    number   integer,
    id_owner bigserial primary key
);

create table drive
(
    id_driver    bigserial primary key,
    id_car       integer,
    date         timestamp,
    length_drive integer

);

create table пользователь_car_drive
(
    id_owner_car bigint,
    id_owner     bigint,
    foreign key (id_owner_car) references car (id_owner)
);


create table rent
(
    id_owner_car bigint,
    id_owner     bigint,
    foreign key (id_owner) references пользователь (id_owner_car)
);









