package ru.inno.cars.config;

import java.util.Properties;
import java.io.InputStream;

public class Config {
    private String fileExtension;

    public Config() {
        Properties properties = new Properties();

        try (InputStream input = Config.class.getResourceAsStream("app.properties")) {
            properties.load(input);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

        this.fileExtension = properties.getProperty("file.extension");
    }

    public String getFileExcention() {
        return fileExtension;
    }
}

