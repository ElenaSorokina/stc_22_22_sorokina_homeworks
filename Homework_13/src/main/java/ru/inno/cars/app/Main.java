package ru.inno.cars.app;


import ru.inno.cars.app.models.Car;
import ru.inno.cars.app.repositories.CarsRepository;
import ru.inno.cars.app.repositories.CarsRepositoryJdbcImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
            Connection connection = DriverManager.getConnection(
                    dbProperties.getProperty("db.url"),
                    dbProperties.getProperty("db.username"),
                    dbProperties.getProperty("db.password"));

            CarsRepository carsRepository = new CarsRepositoryJdbcImpl(connection);
            List<Car> cars = carsRepository.findAll();
            System.out.println(cars);
        } catch (IOException | SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}