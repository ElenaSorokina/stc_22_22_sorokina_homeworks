package ru.inno.cars.app.repositories;

import ru.inno.cars.app.models.Car;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CarsRepositoryJdbcImpl implements CarsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from car order by id_owner";



    private Connection connection;

    public CarsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Car car = Car.builder()
                            .color(resultSet.getString("color"))
                            .id_owner(resultSet.getLong("id_owner"))
                            .model(resultSet.getString("model"))
                            .number(resultSet.getLong("number"))
                            .build();

                    cars.add(car);
                }
            }
    } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return cars;
    }
}
