package ru.inno.cars.app.repositories;

import ru.inno.cars.app.models.Car;

import java.util.List;

public interface CarsRepository {
    List<Car> findAll();

}
