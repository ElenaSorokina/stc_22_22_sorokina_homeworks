package ru.inno.cars.app.models;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class Car {
    private Long id_owner;
    private String model;
    private String color;
    private Long number;

}
