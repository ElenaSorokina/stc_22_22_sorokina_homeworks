import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Main {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("src/input2.txt");
            char[] characters = new char[350];
            characters[0] = (char) reader.read();
            characters[1] = (char) reader.read();
            characters[2] = (char) reader.read();
            characters[3] = (char) reader.read();
            characters[4] = (char) reader.read();
            characters[5] = (char) reader.read();
            characters[6] = (char) reader.read();
            characters[7] = (char) reader.read();
            characters[8] = (char) reader.read();
            characters[9] = (char) reader.read();
            System.out.println(reader.read());
            int i = 0;
        } catch (FileNotFoundException e) {
            System.out.println("Ошибки " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Ошибки в процессе чтения");
        }
    }
}