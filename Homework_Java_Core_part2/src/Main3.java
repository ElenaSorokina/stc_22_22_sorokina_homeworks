import java.io.IOException;

public class Main3 {
    public static void main(String[] args) throws IOException {
        ProductRepository productRepository = new ProductRepositoryFileBasedImpl("src/input2.txt");

        ProductService productService = new ProductService(productRepository);

        System.out.println(productService.countOfAllProduct());
        System.out.println(productRepository.findById(2));
        System.out.println(productRepository.findAllByTitleLike("ан"));

    }
}