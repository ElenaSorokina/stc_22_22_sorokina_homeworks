import java.io.IOException;

public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public int countOfAllProduct() throws IOException {
        return productRepository.findAll().size();

    }

}
