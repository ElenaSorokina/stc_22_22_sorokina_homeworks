import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.lang.Integer.parseInt;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = new Function<String, Product>() {
        @Override
        public Product apply(String currentProduct) {
            String[] parts = currentProduct.split("\\|");
            Integer id = parseInt(parts[0]);
            String name = parts[1];
            Integer price = Integer.valueOf(parts[2]);
            Integer amount = parseInt(parts[3]);
            return new Product(id, name, amount, price);
        }
    };


    @Override
    public Product findById(Integer id) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .findFirst()
                    .orElse(new Product(0, "", 0, 0));
        } catch (IOException e) {
            throw new IOException();
        }

    }


    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String currentProduct = bufferedReader.readLine();

            while (currentProduct != null) {
                Product product = stringToProductMapper.apply(currentProduct);
                products.add(product);
                currentProduct = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileException(e);
        }
        return products;
    }

    @Override
    public List<Product> findAllByTitleLike(String title) throws IOException {
        return null;
    }

    public List<Product> findById(String id) throws IOException {
        return null;
    }

    @Override
    public void update(Product product) throws IOException {

    }
}
