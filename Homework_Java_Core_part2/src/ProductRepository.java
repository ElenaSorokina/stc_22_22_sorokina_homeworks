import java.io.IOException;
import java.util.List;

public interface ProductRepository {
    default List<Product> findAll() {
        return null;
    }

    default Product findById(Integer id) throws IOException {
        return null;
    }


    default List<Product> findAllByTitleLike(String title) throws IOException {
        return null;
    }


    void update(Product product) throws IOException;
}